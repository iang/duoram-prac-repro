void concatanate_index(char const * filename, char tmp[100], size_t index)
{
	 strcpy(tmp, filename);
  strcat(tmp, std::to_string(index).c_str());
}


int read_final_correction_word(bool party, DB_t& FCW_read, int i = 0)
{
  if(!party)
		{
   char tmp[100];
   concatanate_index("../duoram-online/preprocflags/FCW0", tmp, i);
			int const in0 { open(tmp, O_RDONLY ) };
	 	size_t r = read(in0, &FCW_read,   sizeof(FCW_read));	
	 	close(in0);
   if(r < 0) perror("Read error");
		}
		
  if(party)
		{
   char tmp[100];
   concatanate_index("../duoram-online/preprocflags/FCW1", tmp, i);
		 int const in0 { open(tmp, O_RDONLY ) };
	 	size_t r = read(in0, &FCW_read,   sizeof(FCW_read));
	 		close(in0);	
 		if(r < 0) perror("Read error");
		}

  return 0;
}

int read_rand_indx(bool party, DB_t& R, int i = 0)
{
  if(!party)
		{
   char tmp[100];
   concatanate_index("../duoram-online/preprocflags/R0", tmp, i);
			int const in0 { open(tmp, O_RDONLY ) };
	 	size_t r = read(in0, &R,   sizeof(R));
	 	close(in0);	
   if(r < 0) perror("Read error");
		}
		
  if(party)
		{
   char tmp[100];
   concatanate_index("../duoram-online/preprocflags/R1", tmp, i);
		 int const in0 { open(tmp, O_RDONLY ) };
	 	size_t r = read(in0, &R,   sizeof(R));	
	 	close(in0);
 		if(r < 0) perror("Read error");
		}

  return 0;
}

int read_flags_for_reading(bool party, size_t db_nitems, int i = 0)
{
	if(!party)
	{
   char tmp[100];
   concatanate_index("../duoram-online/preprocflags/party0_read_flags_b", tmp, i);
	 	int const in0 { open(tmp, O_RDONLY ) };
	 	size_t r = read(in0, reading_b,  sizeof(reading_b));	
	 	close(in0);
   if(r < 0) perror("Read error");
  
   concatanate_index("../duoram-online/preprocflags/party0_read_flags_c", tmp, i);
   int const in1 { open( tmp, O_RDONLY ) };
	 	r = read(in1, reading_c,  sizeof(reading_c));
	 	close(in1);
   if(r < 0) perror("Read error");

   concatanate_index("../duoram-online/preprocflags/party0_read_flags_d", tmp, i);
	  int const in2 { open( tmp, O_RDONLY ) };
	  r = read(in2, reading_d,  sizeof(reading_d));
	  close(in2);	
   if(r < 0) perror("Read error");
	}

	if(party)
	{
   char tmp[100];
  
   concatanate_index("../duoram-online/preprocflags/party1_read_flags_b", tmp, i);
		 int const in0 { open(tmp, O_RDONLY ) };
	 	size_t r = read(in0, reading_b,  sizeof(reading_b));	
	 	close(in0);
	 	if(r < 0) perror("Read error");

   concatanate_index("../duoram-online/preprocflags/party1_read_flags_c", tmp, i);
 	 int const in1 { open(tmp, O_RDONLY ) };
		 r = read(in1, reading_c,  sizeof(reading_c));
		 close(in1);
		 if(r < 0) perror("Read error");
    
   concatanate_index("../duoram-online/preprocflags/party1_read_flags_d", tmp, i);
		 int const in2 { open(tmp, O_RDONLY ) };
		 r = read(in2, reading_d,  sizeof(reading_d));	
		 close(in2);
   if(r < 0) perror("Read error");
}

 
	return 0;	
}

int read_flags_for_writing(bool party, size_t db_nitems, int i = 0)
{
	if(!party)
	{
    char tmp[100];
    concatanate_index("../duoram-online/preprocflags/party0_write_flags_b", tmp, i);
	   int const in0_w { open(tmp, O_RDONLY ) };
    size_t r = read(in0_w, writing_b,  sizeof(writing_b));
    close(in0_w);	
    if(r < 0) perror("Read error");

    concatanate_index("../duoram-online/preprocflags/party0_write_flags_c", tmp,i);	   
	   int const in1_w { open( tmp, O_RDONLY ) };
    r = read(in1_w, writing_c,  sizeof(writing_c));
    close(in1_w);
    if(r < 0) perror("Read error");
	
    concatanate_index("../duoram-online/preprocflags/party0_write_flags_d", tmp,i);
	   int const in2_w { open( tmp, O_RDONLY ) };
   	r = read(in2_w, writing_d,  sizeof(writing_d));	
   	close(in2_w);
    if(r < 0) perror("Read error");
	}

	if(party)
	{
   char tmp[100];
   concatanate_index("../duoram-online/preprocflags/party1_write_flags_b", tmp,i);
 		int const in0_w { open( tmp, O_RDONLY ) };
	 	size_t r = read(in0_w, writing_b,  sizeof(writing_b));	
	 	close(in0_w);
	 	if(r < 0) perror("Read error");

   concatanate_index("../duoram-online/preprocflags/party1_write_flags_c", tmp, i);
   int const in1_w { open(tmp, O_RDONLY ) };
		 r = read(in1_w, writing_c,  sizeof(writing_c));
		 close(in1_w);
   if(r < 0) perror("Read error");

		 concatanate_index("../duoram-online/preprocflags/party1_write_flags_d", tmp, i);
		 int const in2_w { open( tmp, O_RDONLY ) };
		 r = read(in2_w, writing_d,  sizeof(writing_d));	
		 close(in2_w);
	  if(r < 0) perror("Read error");
	}

	 return 0;	
}

 int read_flags_for_refreshing(size_t db_nitems, int i = 0)
 {
   char tmp[100];
   concatanate_index("../duoram-online/preprocflags/P2_party0_write_flags_c", tmp, i);
   int const in1_w { open(tmp, O_RDONLY ) };
   size_t r = read(in1_w, writing_c,  sizeof(writing_c));
    close(in1_w);
   if(r < 0) perror("Read error");
   
   concatanate_index("../duoram-online/preprocflags/P2_party1_write_flags_d", tmp, i);
   int const in2_w { open(tmp, O_RDONLY ) };
   r = read(in2_w, writing_d,  sizeof(writing_d)); 
   close(in2_w);
   if(r < 0) perror("Read error");

   concatanate_index("../duoram-online/preprocflags/P2_party0_write_c", tmp, i);
   int const in1_w_ { open(tmp, O_RDONLY ) };
   r = read(in1_w_, c,  sizeof(c));
   close(in1_w_);
   if(r < 0) perror("Read error");
   
   concatanate_index("../duoram-online/preprocflags/P2_party1_write_d", tmp, i);
   int const in2_w_ { open(tmp, O_RDONLY ) };
   r = read(in2_w_, d,  sizeof(d));  
   close(in2_w_);
   if(r < 0) perror("Read error");

   return 0;
 }

int read_flags_for_generating_cancellation_terms(size_t db_nitems, int i = 0)
{
  char tmp[100];
  concatanate_index("../duoram-online/preprocflags/P2_party1_read_flags_d", tmp, i);
  int const in2 { open(tmp, O_RDONLY ) };
  size_t r = read(in2, reading_d,  sizeof(reading_d)); 
  close(in2);
  if(r < 0) perror("Read error");

  concatanate_index("../duoram-online/preprocflags/P2_party0_read_flags_c", tmp, i);
  int const in2_ { open(tmp, O_RDONLY ) };
  r = read(in2_, reading_c,  sizeof(reading_c)); 
  close(in2_);	
  if(r < 0) perror("Read error");
   
  return 0;
}


int read_flags_for_updating(bool party, size_t db_nitems, int i = 0)
{
 char tmp[100];
	if(!party)
	{
  concatanate_index("../duoram-online/preprocflags/party0_write_b", tmp, i);
		int const in0_w_ { open(tmp, O_RDONLY ) };
		size_t r = read(in0_w_, b,  sizeof(b));	
		close(in0_w_);	
	 if(r < 0) perror("Read error");
 	
  concatanate_index("../duoram-online/preprocflags/party0_write_c", tmp, i);
  int const in1_w_ { open(tmp, O_RDONLY ) };
		r = read(in1_w_, c,  sizeof(c));
		close(in1_w_);	
  if(r < 0) perror("Read error");

  concatanate_index("../duoram-online/preprocflags/party0_write_d", tmp, i); 
  int const in2_w_ { open(tmp, O_RDONLY ) };
	 r = read(in2_w_, d,  sizeof(d));	
	 close(in2_w_);	
  if(r < 0) perror("Read error");
	}

	if(party)
	{
  concatanate_index("../duoram-online/preprocflags/party1_write_b", tmp, i);
 	int const in0_w_ { open( tmp, O_RDONLY ) };
	 size_t r = read(in0_w_, b,  sizeof(b));
	 close(in0_w_);	
  if(r < 0) perror("Read error");
 	
  concatanate_index("../duoram-online/preprocflags/party1_write_c", tmp, i);
  int const in1_w_ { open(tmp, O_RDONLY ) };
		r = read(in1_w_, c,  sizeof(c));
	 close(in1_w_);	
  if(r < 0) perror("Read error");

  concatanate_index("../duoram-online/preprocflags/party1_write_d", tmp, i); 	
  int const in2_w_ { open(tmp, O_RDONLY ) };
		 r = read(in2_w_, d,  sizeof(d));
		 close(in2_w_);		
  if(r < 0) perror("Read error");
	}

 
	return 0;	
}