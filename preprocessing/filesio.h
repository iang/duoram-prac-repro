/**
 * @brief The function writes the flag vectors into /preprocflags/ directory in duoram-online
 * 
 * @param party 
 * @param i 
 * @param db_nitems 
 * @param flags 
 * @param outs_ 
 * @param final_correction_word 
 */

void write_evalfull_outs_into_a_file(bool party, size_t i, size_t db_nitems,  int8_t * flags, int64_t * outs_, __m128i  final_correction_word, int64_t additve_sharesR)
{
 if(!party) 
 {  
   char const * p0_filename0;
	  char tmp[100];
   p0_filename0 = "../duoram-online/preprocflags/party0_read_flags_b";
	  strcpy(tmp, p0_filename0);
 	 strcat(tmp, std::to_string(i).c_str());
	  int w0 = open( tmp,  O_WRONLY | O_APPEND | O_CREAT, S_IWRITE | S_IREAD);
   int written = write(w0, flags, db_nitems * sizeof(flags[0]));
   if(written<0)  perror("Write error"); 
   close(w0);

 	 p0_filename0 = "../duoram-online/preprocflags/party0_read_flags_c";
   strcpy(tmp, p0_filename0);
 	 strcat(tmp, std::to_string(i).c_str());
   int w1 = open( tmp,  O_WRONLY | O_APPEND | O_CREAT, S_IWRITE | S_IREAD);
   written = write(w1, flags, db_nitems * sizeof(flags[0]));
   if(written<0)  perror("Write error"); 
   close(w1);
	
	  p0_filename0 = "../duoram-online/preprocflags/party0_read_flags_d";
	  strcpy(tmp, p0_filename0);
 	 strcat(tmp, std::to_string(i).c_str());
   int w2 = open( tmp,  O_WRONLY | O_APPEND | O_CREAT, S_IWRITE | S_IREAD);
   written = write(w2, flags, db_nitems * sizeof(flags[0]));   
	  if(written<0)  perror("Write error"); 
	  close(w2);


 	 p0_filename0 = "../duoram-online/preprocflags/party0_write_flags_b";
	 	strcpy(tmp, p0_filename0);
 	 strcat(tmp, std::to_string(i).c_str());
   int w4 = open( tmp,  O_WRONLY | O_APPEND | O_CREAT, S_IWRITE | S_IREAD);
   written = write(w4, flags, db_nitems * sizeof(flags[0]));
   if(written<0)  perror("Write error"); 
   close(w4);

   p0_filename0 = "../duoram-online/preprocflags/party0_write_flags_c";
   strcpy(tmp, p0_filename0);
 	 strcat(tmp, std::to_string(i).c_str());
   int w5 = open( tmp,  O_WRONLY | O_APPEND | O_CREAT, S_IWRITE | S_IREAD);
   written = write(w5, flags, db_nitems * sizeof(flags[0]));
   if(written<0)  perror("Write error"); 
   close(w5);
	  
   p0_filename0 = "../duoram-online/preprocflags/party0_write_flags_d";
	  strcpy(tmp, p0_filename0);
 	 strcat(tmp, std::to_string(i).c_str());
   int w6 = open( tmp,  O_WRONLY | O_APPEND | O_CREAT, S_IWRITE | S_IREAD);
   written = write(w6, flags, db_nitems * sizeof(flags[0]));
   if(written<0)  perror("Write error");    
	  close(w6);

	  p0_filename0 = "../duoram-online/preprocflags/party0_write_b";
	 	strcpy(tmp, p0_filename0);
 	 strcat(tmp, std::to_string(i).c_str());
   w4 = open( tmp,  O_WRONLY | O_APPEND | O_CREAT, S_IWRITE | S_IREAD);
   written = write(w4, outs_, db_nitems * sizeof(outs_[0])); 
   if(written<0)  perror("Write error"); 
   close(w4);

 	 p0_filename0 = "../duoram-online/preprocflags/party0_write_c";
	  strcpy(tmp, p0_filename0);
 	 strcat(tmp, std::to_string(i).c_str());
   w5 = open( tmp,  O_WRONLY | O_APPEND | O_CREAT, S_IWRITE | S_IREAD);
   written = write(w5, outs_, db_nitems * sizeof(outs_[0]));
   if(written<0)  perror("Write error"); 
   close(w5);

   p0_filename0 = "../duoram-online/preprocflags/party0_write_d";
		 strcpy(tmp, p0_filename0);
 	 strcat(tmp, std::to_string(i).c_str());
   w6 = open( tmp,  O_WRONLY | O_APPEND | O_CREAT, S_IWRITE | S_IREAD);
   written = write(w6, outs_, db_nitems * sizeof(outs_[0]));   
   if(written<0)  perror("Write error");       
	  close(w6);	  
  }

  if(party) 
  {
 	 char const * p0_filename0;
	  char tmp[100];
   p0_filename0 = "../duoram-online/preprocflags/party1_read_flags_b";
	  strcpy(tmp, p0_filename0);
 	 strcat(tmp, std::to_string(i).c_str());
   int w0 = open( tmp,  O_WRONLY | O_APPEND | O_CREAT, S_IWRITE | S_IREAD);
   int written = write(w0, flags, db_nitems * sizeof(flags[0]));
   if(written<0)  perror("Write error"); 
   close(w0);	

	  p0_filename0 = "../duoram-online/preprocflags/party1_read_flags_c";
	  strcpy(tmp, p0_filename0);
 	 strcat(tmp, std::to_string(i).c_str());	 
	  int w1 = open( tmp,  O_WRONLY | O_APPEND | O_CREAT, S_IWRITE | S_IREAD);
	  written = write(w1, flags, db_nitems * sizeof(flags[0]));
   if(written<0)  perror("Write error"); 
   close(w1);

	  p0_filename0 = "../duoram-online/preprocflags/party1_read_flags_d";
	  strcpy(tmp, p0_filename0);
 	 strcat(tmp, std::to_string(i).c_str()); 	  
	  int w2 = open( tmp,  O_WRONLY | O_APPEND | O_CREAT, S_IWRITE | S_IREAD);
   written = write(w2,flags, db_nitems * sizeof(flags[0]));    
   if(written<0)  perror("Write error");      
   close(w2);


	  p0_filename0 = "../duoram-online/preprocflags/party1_write_flags_b";
	  strcpy(tmp, p0_filename0);
 	 strcat(tmp, std::to_string(i).c_str());  
	  int w4 = open( tmp,  O_WRONLY | O_APPEND | O_CREAT, S_IWRITE | S_IREAD);
   written = write(w4,flags, db_nitems * sizeof(flags[0]));
   if(written<0)  perror("Write error"); 
   close(w4);

   p0_filename0 = "../duoram-online/preprocflags/party1_write_flags_c";
	  strcpy(tmp, p0_filename0);
 	 strcat(tmp, std::to_string(i).c_str());
   int w5 = open( tmp,  O_WRONLY | O_APPEND | O_CREAT, S_IWRITE | S_IREAD);
   written = write(w5, flags, db_nitems * sizeof(flags[0]));
   if(written<0)  perror("Write error"); 
   close(w5);
	
  	p0_filename0 = "../duoram-online/preprocflags/party1_write_flags_d";
  	strcpy(tmp, p0_filename0);
 	 strcat(tmp, std::to_string(i).c_str());	   
	  int w6 = open( tmp,  O_WRONLY | O_APPEND | O_CREAT, S_IWRITE | S_IREAD);
   written = write(w6, flags, db_nitems * sizeof(flags[0]));    
   if(written<0)  perror("Write error");    
	  close(w6);

	  p0_filename0 = "../duoram-online/preprocflags/party1_write_b";
	  strcpy(tmp, p0_filename0);
 	 strcat(tmp, std::to_string(i).c_str());
   w4 = open( tmp,  O_WRONLY | O_APPEND | O_CREAT, S_IWRITE | S_IREAD);
   written = write(w4, outs_, db_nitems * sizeof(outs_[0]));
   if(written<0)  perror("Write error"); 
   close(w4);

   p0_filename0 = "../duoram-online/preprocflags/party1_write_c";
   strcpy(tmp, p0_filename0);
 	 strcat(tmp, std::to_string(i).c_str());
   w5 = open( tmp,  O_WRONLY | O_APPEND | O_CREAT, S_IWRITE | S_IREAD);
   written = write(w5, outs_, db_nitems * sizeof(outs_[0]));
   if(written<0)  perror("Write error"); 
   close(w5);
	   
   p0_filename0 = "../duoram-online/preprocflags/party1_write_d";
	  strcpy(tmp, p0_filename0);
 	 strcat(tmp, std::to_string(i).c_str());
   w6 = open( tmp,  O_WRONLY | O_APPEND | O_CREAT, S_IWRITE | S_IREAD);
   written = write(w6, outs_, db_nitems * sizeof(outs_[0]));    
	  if(written<0)  perror("Write error");    
	  close(w6);

  }

  if(party)
	 {
		 char const * p1_filename0;
		 char tmp[100];
		 p1_filename0 = "../duoram-online/preprocflags/FCW1";
	 	strcpy(tmp, p1_filename0);
 		strcat(tmp, std::to_string(i).c_str());
		
		 int w0 = open( tmp,  O_WRONLY | O_APPEND | O_CREAT, S_IWRITE | S_IREAD);
		 int written = write(w0, &final_correction_word[0],  sizeof(uint64_t));
		 if(written<0)	perror("Write error");
   close(w0);
   
   p1_filename0 = "../duoram-online/preprocflags/R1";
	 	strcpy(tmp, p1_filename0);
 		strcat(tmp, std::to_string(i).c_str());
		
		 w0 = open( tmp,  O_WRONLY | O_APPEND | O_CREAT, S_IWRITE | S_IREAD);
		 written = write(w0, &additve_sharesR,  sizeof(int64_t));
		 if(written<0)	perror("Write error");

		 close(w0);
	}
	
	if(!party)
	{
		char const * p0_filename0;
		char tmp[100];
		p0_filename0 = "../duoram-online/preprocflags/FCW0";
		strcpy(tmp, p0_filename0);
 	strcat(tmp, std::to_string(i).c_str());
		
		int w0 = open( tmp,  O_WRONLY | O_APPEND | O_CREAT, S_IWRITE | S_IREAD);
		int written = write(w0, &final_correction_word[0],  sizeof(uint64_t));
		if(written<0) perror("Write error");
		close(w0);

  p0_filename0 = "../duoram-online/preprocflags/R0";
	 strcpy(tmp, p0_filename0);
 	strcat(tmp, std::to_string(i).c_str());
		
		w0 = open( tmp,  O_WRONLY | O_APPEND | O_CREAT, S_IWRITE | S_IREAD);
		written = write(w0, &additve_sharesR,  sizeof(int64_t));
		if(written<0)	perror("Write error");

		 close(w0);
	}
 
}

void P2_write_evalfull_outs_into_a_file(bool party, size_t i, size_t db_nitems,  int8_t * flags, int64_t * outs_)
{
 if(!party) 
 {  
   char const * p0_filename0;
	  char tmp[100];
   p0_filename0 = "../duoram-online/preprocflags/P2_party0_read_flags_b";
	  strcpy(tmp, p0_filename0);
 	 strcat(tmp, std::to_string(i).c_str());
	
	  int w0 = open( tmp,  O_WRONLY | O_APPEND | O_CREAT, S_IWRITE | S_IREAD);
   int written = write(w0, flags, db_nitems * sizeof(flags[0]));
   if(written<0) perror("Write error");
   close(w0);

 	 p0_filename0 = "../duoram-online/preprocflags/P2_party0_read_flags_c";
   strcpy(tmp, p0_filename0);
 	 strcat(tmp, std::to_string(i).c_str());
   int w1 = open( tmp,  O_WRONLY | O_APPEND | O_CREAT, S_IWRITE | S_IREAD);
   written = write(w1, flags, db_nitems * sizeof(flags[0]));
   if(written<0) perror("Write error");
   close(w1);
	
	  p0_filename0 = "../duoram-online/preprocflags/P2_party0_read_flags_d";
	  strcpy(tmp, p0_filename0);
   strcat(tmp, std::to_string(i).c_str());
   int w2 = open( tmp,  O_WRONLY | O_APPEND | O_CREAT, S_IWRITE | S_IREAD);
   written = write(w2, flags, db_nitems * sizeof(flags[0]));
   if(written<0) perror("Write error");
   close(w2);

 	 p0_filename0 = "../duoram-online/preprocflags/P2_party0_write_flags_b";
	 	strcpy(tmp, p0_filename0);
 	 strcat(tmp, std::to_string(i).c_str());
   int w4 = open( tmp,  O_WRONLY | O_APPEND | O_CREAT, S_IWRITE | S_IREAD);
   written = write(w4, flags, db_nitems * sizeof(flags[0]));
   if(written<0) perror("Write error");
   close(w4);

   p0_filename0 = "../duoram-online/preprocflags/P2_party0_write_flags_c";
   strcpy(tmp, p0_filename0);
 	 strcat(tmp, std::to_string(i).c_str());
   int w5 = open( tmp,  O_WRONLY | O_APPEND | O_CREAT, S_IWRITE | S_IREAD);
   written = write(w5, flags, db_nitems * sizeof(flags[0]));
   if(written<0) perror("Write error");
   close(w5);
	  
   p0_filename0 = "../duoram-online/preprocflags/P2_party0_write_flags_d";
	  strcpy(tmp, p0_filename0);
 	 strcat(tmp, std::to_string(i).c_str());
   int w6 = open( tmp,  O_WRONLY | O_APPEND | O_CREAT, S_IWRITE | S_IREAD);
   written = write(w6, flags, db_nitems * sizeof(flags[0]));
   if(written<0) perror("Write error"); 
	  close(w6);

   p0_filename0 = "../duoram-online/preprocflags/P2_party0_write_b";
   strcpy(tmp, p0_filename0);
   strcat(tmp, std::to_string(i).c_str());
   w4 = open( tmp,  O_WRONLY | O_APPEND | O_CREAT, S_IWRITE | S_IREAD);
   written = write(w4, outs_, db_nitems * sizeof(outs_[0]));
   if(written<0) perror("Write error"); 
   close(w4);

 	 p0_filename0 = "../duoram-online/preprocflags/P2_party0_write_c";
	  strcpy(tmp, p0_filename0);
 	 strcat(tmp, std::to_string(i).c_str());
   w5 = open( tmp,  O_WRONLY | O_APPEND | O_CREAT, S_IWRITE | S_IREAD);
   written = write(w5, outs_, db_nitems * sizeof(outs_[0]));
   if(written<0) perror("Write error"); 
   close(w5);

   p0_filename0 = "../duoram-online/preprocflags/P2_party0_write_d";
		 strcpy(tmp, p0_filename0);
   strcat(tmp, std::to_string(i).c_str());
   w6 = open( tmp,  O_WRONLY | O_APPEND | O_CREAT, S_IWRITE | S_IREAD);
   written = write(w6, outs_, db_nitems * sizeof(outs_[0]));
   if(written<0) perror("Write error"); 
 	 close(w6);	  
  }
  
  if(party) 
  {
	   char const * p0_filename0;
	   char tmp[100];
    p0_filename0 = "../duoram-online/preprocflags/P2_party1_read_flags_b";
	   strcpy(tmp, p0_filename0);
 	  strcat(tmp, std::to_string(i).c_str());
     
	   int w0 = open( tmp,  O_WRONLY | O_APPEND | O_CREAT, S_IWRITE | S_IREAD);
    int written = write(w0, flags, db_nitems * sizeof(flags[0]));
    if(written<0) perror("Write error");   
	   close(w0);	

	   p0_filename0 = "../duoram-online/preprocflags/P2_party1_read_flags_c";
	   strcpy(tmp, p0_filename0);
 	  strcat(tmp, std::to_string(i).c_str());
   	int w1 = open( tmp,  O_WRONLY | O_APPEND | O_CREAT, S_IWRITE | S_IREAD);
	   written = write(w1, flags, db_nitems * sizeof(flags[0]));
	   if(written<0)  perror("Write error");
    close(w1);

	   p0_filename0 = "../duoram-online/preprocflags/P2_party1_read_flags_d";
	   strcpy(tmp, p0_filename0);
 	  strcat(tmp, std::to_string(i).c_str());  
	   int w2 = open( tmp,  O_WRONLY | O_APPEND | O_CREAT, S_IWRITE | S_IREAD);
    written = write(w2,flags, db_nitems * sizeof(flags[0]));
    if(written<0)  perror("Write error"); 
    close(w2);

	   p0_filename0 = "../duoram-online/preprocflags/P2_party1_write_flags_b";
	   strcpy(tmp, p0_filename0);
 	  strcat(tmp, std::to_string(i).c_str());
	   int w4 = open( tmp,  O_WRONLY | O_APPEND | O_CREAT, S_IWRITE | S_IREAD);
    written = write(w4,flags, db_nitems * sizeof(flags[0]));
    if(written<0)  perror("Write error"); 
    close(w4);

    p0_filename0 = "../duoram-online/preprocflags/P2_party1_write_flags_c";
	   strcpy(tmp, p0_filename0);
 	  strcat(tmp, std::to_string(i).c_str());
	   int w5 = open( tmp,  O_WRONLY | O_APPEND | O_CREAT, S_IWRITE | S_IREAD);
    written = write(w5, flags, db_nitems * sizeof(flags[0]));
    if(written<0)  perror("Write error"); 
    close(w5);
	
	   p0_filename0 = "../duoram-online/preprocflags/P2_party1_write_flags_d";
   	strcpy(tmp, p0_filename0);
 	  strcat(tmp, std::to_string(i).c_str());   
	   int w6 = open( tmp,  O_WRONLY | O_APPEND | O_CREAT, S_IWRITE | S_IREAD);
   	written = write(w6, flags, db_nitems * sizeof(flags[0]));  
    if(written<0)  perror("Write error");   
   	close(w6);

  	 p0_filename0 = "../duoram-online/preprocflags/P2_party1_write_b";
	   strcpy(tmp, p0_filename0);
 	  strcat(tmp, std::to_string(i).c_str());
    w4 = open( tmp,  O_WRONLY | O_APPEND | O_CREAT, S_IWRITE | S_IREAD);
    written = write(w4, outs_, db_nitems * sizeof(outs_[0]));
    if(written<0)  perror("Write error"); 
    close(w4);

 	//char const * p0_filename0;
    p0_filename0 = "../duoram-online/preprocflags/P2_party1_write_c";
   	strcpy(tmp, p0_filename0);
 	  strcat(tmp, std::to_string(i).c_str());
    w5 = open( tmp,  O_WRONLY | O_APPEND | O_CREAT, S_IWRITE | S_IREAD);
    written = write(w5, outs_, db_nitems * sizeof(outs_[0]));
    if(written<0)  perror("Write error"); 
    close(w5);
	
	   p0_filename0 = "../duoram-online/preprocflags/P2_party1_write_d";
	   strcpy(tmp, p0_filename0);
 	  strcat(tmp, std::to_string(i).c_str());
    w6 = open( tmp,  O_WRONLY | O_APPEND | O_CREAT, S_IWRITE | S_IREAD);
    written = write(w6, outs_, db_nitems * sizeof(outs_[0]));
    if(written<0)  perror("Write error");   
	   close(w6);
  } 
}






