struct dpfP2
{
	__m128i root;
	__m128i CW[32];
	uint8_t cwt_L[32];
	uint8_t cwt_R[32];
	};

void generate_random_targets(uint8_t ** target_share_read, size_t n_threads, bool party, size_t expo)
{
 for(size_t i = 0; i < n_threads; i++)
	{
		target_share_read[i] = new uint8_t[64];
	}

	for(size_t j = 0; j < 64; ++j)
	{	
 		for(size_t i = 0; i < n_threads; ++i)
		{
			uint8_t random_value;
			arc4random_buf(&random_value, sizeof(uint8_t));
			target_share_read[i][j] = random_value;//rand();
			target_share_read[i][j] = target_share_read[i][j] % 2;
			//if(party) target_share_read[i][expo-2] = 1;
		}
	}
}

struct cw_construction
{
  __m128i rand_b, gamma_b;
  uint8_t bit_b;
};

struct BlindsCW
{
	__m128i blinded_message;
	uint8_t blinded_bit;
};

 struct reconstructioncw
 {
  __m128i cw;
  uint8_t cwbit[2];
 };

 

void compute_CW(cw_construction computecw_array, size_t ind, size_t layer,tcp::socket& sout,  __m128i L, __m128i R, uint8_t bit, __m128i & CW, uint8_t &cwt_L, uint8_t &cwt_R, size_t &thread_communication_cost)
{

 	reconstructioncw cwsent, cwrecv;
	__m128i rand_b  = computecw_array.rand_b;		//   computecw.rand_b;
	__m128i gamma_b = computecw_array.gamma_b;	//   computecw.gamma_b;
	uint8_t bit_b   = computecw_array.bit_b;		//   computecw.bit_b;
	
	uint8_t blinded_bit, blinded_bit_read;
	blinded_bit = bit ^ bit_b;

	__m128i blinded_L = L ^ R ^ rand_b;
	__m128i blinded_L_read; 

	BlindsCW blinds_sent, blinds_recv;
	blinds_sent.blinded_bit = blinded_bit;
	blinds_sent.blinded_message = blinded_L;

	//exchange blinded shares for OSWAP.
  boost::asio::write(sout, boost::asio::buffer(&blinds_sent, sizeof(BlindsCW)));
  thread_communication_cost += sizeof(BlindsCW);

	boost::asio::read(sout, boost::asio::buffer(&blinds_recv, sizeof(BlindsCW)));
	
	blinded_bit_read = blinds_recv.blinded_bit;
	blinded_L_read   =  blinds_recv.blinded_message;

  cwsent.cw = R ^ gamma_b;

	if(bit)
	{
	  cwsent.cw ^= (L ^ R ^ blinded_L_read);
	}
	if(blinded_bit_read)
	{
	  cwsent.cw ^= rand_b;
	}

 
 cwsent.cwbit[0] = get_lsb(L) ^ bit;//advice[0];
 cwsent.cwbit[1] = get_lsb(R) ^ bit;//advice[1];

 boost::asio::write(sout, boost::asio::buffer(&cwsent, sizeof(cwsent)));
 boost::asio::read(sout, boost::asio::buffer(&cwrecv, sizeof(cwrecv)));
 thread_communication_cost += sizeof(cwsent);
 cwrecv.cw ^= cwsent.cw;
 cwrecv.cwbit[0] ^= (cwsent.cwbit[0] ^ 1);
 cwrecv.cwbit[1] ^= (cwsent.cwbit[1]);
 
 cwt_L = cwrecv.cwbit[0];
 cwt_R = cwrecv.cwbit[1];
 CW = cwrecv.cw;

 // The following asserts the correctness of ComputeCW
	#ifdef DEBUG
			uint8_t bit_reconstruction; 
			boost::asio::write(sout, boost::asio::buffer(&bit, sizeof(bit)));
			boost::asio::read(sout, boost::asio::buffer(&bit_reconstruction, sizeof(bit_reconstruction)));
			bit_reconstruction = bit ^ bit_reconstruction;

			__m128i L_reconstruction; 
			boost::asio::write(sout, boost::asio::buffer(&L, sizeof(L)));
			boost::asio::read(sout, boost::asio::buffer(&L_reconstruction, sizeof(L_reconstruction)));
			L_reconstruction = L ^ L_reconstruction;

			__m128i R_reconstruction; 
			boost::asio::write(sout, boost::asio::buffer(&R, sizeof(R)));
			boost::asio::read(sout, boost::asio::buffer(&R_reconstruction, sizeof(R_reconstruction)));
			R_reconstruction = R ^ R_reconstruction;

			__m128i CW_debug;

			if(bit_reconstruction != 0)
			{
			 CW_debug = L_reconstruction;
			}
			else
			{
			 CW_debug = R_reconstruction;
			}

			assert(CW_debug[0] == CW[0]);
			assert(CW_debug[1] == CW[1]);
	#endif
}


template<typename node_t, typename prgkey_t>
static inline void traverse(const prgkey_t & prgkey, const node_t & seed,	node_t s[2])
{	
	dpf::PRG(prgkey, clear_lsb(seed, 0b11), s, 2);
} // dpf::expand


/**
 * @brief 
 * 
 * @param nodes_per_leaf 
 * @param depth 
 * @param nbits 
 * @param nodes_in_interval 
 * @param prgkey The PRG Key
 * @param target_share 
 * @param socketsPb Array of sockets to write to Pb
 * @param socketsP2 Array of sockets to write to P2
 * @param from 
 * @param to 
 * @param output 
 * @param _t 
 * @param final_correction_word the final correction word is written in to this 
 * @param party Party
 * @param socket_no 
 */
inline void create_dpfs (bool reading,  size_t db_nitems, const AES_KEY& prgkey,  
                         uint8_t target_share[64], std::vector<socket_t>& socketsPb, std::vector<socket_t>& socketsP2, const size_t from, const size_t to, __m128i * output, int8_t * _t, __m128i& final_correction_word,  
						                   cw_construction computecw_array, dpfP2 * dpf_instance, 
                         bool party, size_t socket_no, size_t ind, size_t &thread_communication_cost)
{ 
	const size_t bits_per_leaf = std::is_same<leaf_t, bool>::value ? 1 : sizeof(leaf_t) * CHAR_BIT;
	const bool  is_packed = (sizeof(leaf_t) < sizeof(node_t));
	const size_t nodes_per_leaf = is_packed ? 1 : std::ceil(static_cast<double>(bits_per_leaf) / (sizeof(node_t) * CHAR_BIT));

 const size_t depth = std::ceil(std::log2(db_nitems));
	const size_t nbits = std::ceil(std::log2(db_nitems));
	const size_t nodes_in_interval = db_nitems-1; 
	
	__m128i root;	

	arc4random_buf(&root, sizeof(root));

 root =	set_lsb(root, party);

	const size_t from_node = std::floor(static_cast<double>(from) / nodes_per_leaf);

	__m128i * s[2] = {
	    reinterpret_cast<__m128i *>(output) + nodes_in_interval * (nodes_per_leaf - 1),
	    s[0] + nodes_in_interval / 2
	};
	
	int8_t * t[2] = { _t, _t + nodes_in_interval / 2};

	int curlayer = depth % 2;

	s[curlayer][0] = root;
	t[curlayer][0] = get_lsb(root, 0b01);
	
	__m128i * CW = (__m128i *) std::aligned_alloc(sizeof(__m256i), depth * sizeof(__m128i));
	
	 #ifdef VERBOSE
		if(ind == 0)
		{
			std::cout << "root = "         << root[0] << " " << root[1] << std::endl;
			std::cout << "t[curlayer][0] " << (int) t[curlayer][0] << std::endl;
		}
	 #endif

  dpf_instance[ind].root = root;
	
  // cw_construction computecw_array[15];
  // boost::asio::read(socketsP2[0], boost::asio::buffer(&computecw_array, 15 * sizeof(computecw_array[0])));


  for (size_t layer = 0; layer < depth; ++layer)
	{
			#ifdef VERBOSE	
			printf("layer = %zu\n", layer);
			#endif

			curlayer = 1-curlayer;

			size_t i=0, j=0;
			auto nextbit = (from_node >> (nbits-layer-1)) & 1;
			size_t nodes_in_prev_layer = std::ceil(static_cast<double>(nodes_in_interval) / (1ULL << (depth-layer)));
			size_t nodes_in_cur_layer = std::ceil(static_cast<double>(nodes_in_interval) / (1ULL << (depth-layer-1)));

			__m128i L =  _mm_setzero_si128();
			__m128i R =  _mm_setzero_si128();
			
			for (i = nextbit, j = nextbit; j < nodes_in_prev_layer-1; ++j, i+=2)
			{
				
					traverse(prgkey, s[1-curlayer][j], &s[curlayer][i]);
					L ^= s[curlayer][i];
					R ^= s[curlayer][i+1];
			}
			
			if (nodes_in_prev_layer > j)
			{
				if (i < nodes_in_cur_layer - 1) 
				{
						traverse(prgkey, s[1-curlayer][j], &s[curlayer][i]);
						L ^= s[curlayer][i];
						R ^= s[curlayer][i+1];
				}
			}

  		uint8_t cwt_L, cwt_R;
			
			// Computes the correction word using OSWAP
			compute_CW(computecw_array, ind, layer,  socketsPb[socket_no],  L,  R, target_share[layer], CW[layer], cwt_L,  cwt_R, thread_communication_cost);
			
			#ifdef DEBUG
				if(ind == 0) 
				{ 
					std::cout << "CW reconstruction  = " << CW[layer][0] << " " << CW[layer][1] << std::endl;
					std::cout << "			   cwt_L = " << (int) cwt_L << std::endl;
					std::cout << "			   cwt_R = " << (int) cwt_R << std::endl;
				}
			#endif

			dpf_instance[ind].CW[layer] 		 =  CW[layer];
			dpf_instance[ind].cwt_L[layer]   =  cwt_L;
			dpf_instance[ind].cwt_R[layer]   =  cwt_R;
			
			for(size_t j = 0; j < nodes_in_prev_layer; ++j)
			{	
				t[curlayer][2*j] 	    = get_lsb(s[curlayer][2*j]) ^ (cwt_L & t[1-curlayer][j]);
				s[curlayer][2*j] 	    = clear_lsb(xor_if(s[curlayer][2*j], CW[layer], !t[1-curlayer][j]), 0b01);
				t[curlayer][(2*j)+1]  = get_lsb(s[curlayer][(2*j)+1]) ^ (cwt_R & t[1-curlayer][j]);
				s[curlayer][(2*j)+1]  = clear_lsb(xor_if(s[curlayer][(2*j)+1], CW[layer], !t[1-curlayer][j]), 0b01);
			}
	}
 
  
	__m128i Gamma  =  _mm_setzero_si128();
	 
	for (size_t i = 0; i < to + 1; ++i)
	{
	  Gamma[0] += output[i][0]; // the correction word for duoram update
	  Gamma[1] += output[i][1]; // the correction word for share conversion
	}
		
	if(party) 
	{
	  Gamma[0] = -Gamma[0];  // the correction word for duoram update
	  Gamma[1] = -Gamma[1];  // the correction word for share conversion
	}
	
	#ifdef DEBUG
		boost::asio::write(socketsPb[socket_no + 3], boost::asio::buffer(&Gamma, sizeof(Gamma)));
 		boost::asio::read(socketsPb[socket_no + 3], boost::asio::buffer(&final_correction_word, sizeof(final_correction_word)));
	#endif

 final_correction_word = Gamma;  

 free(CW);

} // create_dpfs



inline void evaluate_dpfs( size_t db_nitems,  dpfP2 dpfinstance,  const AES_KEY& prgkey,   const size_t from, const size_t to, 
							                   __m128i * output, int8_t * _t,  bool party,  size_t ind)
{

 	const size_t bits_per_leaf = std::is_same<leaf_t, bool>::value ? 1 : sizeof(leaf_t) * CHAR_BIT;
	const bool  is_packed = (sizeof(leaf_t) < sizeof(node_t));
	const size_t nodes_per_leaf = is_packed ? 1 : std::ceil(static_cast<double>(bits_per_leaf) / (sizeof(node_t) * CHAR_BIT));

 	const size_t depth = std::ceil(std::log2(db_nitems));
	const size_t nbits = std::ceil(std::log2(db_nitems));
	const size_t nodes_in_interval = db_nitems-1; 
	
	__m128i root = dpfinstance.root;
	__m128i * CW = (__m128i *) std::aligned_alloc(sizeof(__m256i), depth * sizeof(__m128i));
	
	uint8_t * cwt_L = (uint8_t *) std::aligned_alloc(sizeof(__m256i), depth * sizeof(uint8_t));
	uint8_t * cwt_R = (uint8_t *) std::aligned_alloc(sizeof(__m256i), depth * sizeof(uint8_t));
	
	for(size_t j = 0; j < depth; ++j)
	{
		CW[j] 	  = dpfinstance.CW[j];
		cwt_L[j]  = dpfinstance.cwt_L[j];
		cwt_R[j]  = dpfinstance.cwt_R[j];
	}
	
 
 	root =	set_lsb(root, party);

	const size_t from_node = std::floor(static_cast<double>(from) / nodes_per_leaf);

	__m128i * s[2] = {
	    reinterpret_cast<__m128i *>(output) + nodes_in_interval * (nodes_per_leaf - 1),
	    s[0] + nodes_in_interval / 2
};

	int8_t * t[2] = { _t, _t + nodes_in_interval / 2};
	int curlayer = depth % 2;
	s[curlayer][0] = root;
	t[curlayer][0] = get_lsb(root, 0b01);
	
	#ifdef VERBOSE
		if(ind == 0)
		{
			std::cout << "root = "         << root[0] << " " << root[1] << std::endl;
			std::cout << "t[curlayer][0] " << (int) t[curlayer][0] << std::endl;
		}
	#endif

	for (size_t layer = 0; layer < depth; ++layer)
	{
			#ifdef VERBOSE	
				printf("layer = %zu\n", layer);
			#endif

			curlayer = 1-curlayer;

			size_t i=0, j=0;
			auto nextbit = (from_node >> (nbits-layer-1)) & 1;
			size_t nodes_in_prev_layer = std::ceil(static_cast<double>(nodes_in_interval) / (1ULL << (depth-layer)));
			size_t nodes_in_cur_layer = std::ceil(static_cast<double>(nodes_in_interval) / (1ULL << (depth-layer-1)));
			
 			for (i = nextbit, j = nextbit; j < nodes_in_prev_layer-1; ++j, i+=2)
			{
				traverse(prgkey, s[1-curlayer][j], &s[curlayer][i]);
			}
			
			if (nodes_in_prev_layer > j)
			{
				if (i < nodes_in_cur_layer - 1) 
				{
					traverse(prgkey, s[1-curlayer][j], &s[curlayer][i]);
				}
			}

			#ifdef VERBOSE
				if(ind == 0) 
				{ 
					std::cout << "CW reconstruction  = " << CW[layer][0] << " " << CW[layer][1] << std::endl;
					std::cout << "			   cwt_L = " << (int) cwt_L[layer] << std::endl;
					std::cout << "			   cwt_R = " << (int) cwt_R[layer] << std::endl;
				}
			#endif

			for(size_t j = 0; j < nodes_in_prev_layer; ++j)
			{	
				t[curlayer][2*j]   	 = get_lsb(s[curlayer][2*j]) ^ (cwt_L[layer] & t[1-curlayer][j]);
				s[curlayer][2*j] 	   = clear_lsb(xor_if(s[curlayer][2*j], CW[layer], !t[1-curlayer][j]), 0b11);
				t[curlayer][(2*j)+1] = get_lsb(s[curlayer][(2*j)+1]) ^ (cwt_R[layer] & t[1-curlayer][j]);
				s[curlayer][(2*j)+1] = clear_lsb(xor_if(s[curlayer][(2*j)+1], CW[layer], !t[1-curlayer][j]), 0b11);
			}
	}
        free(CW);
        free(cwt_L);
        free(cwt_R);
} // evaluate_dpfs


