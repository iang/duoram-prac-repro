#ifndef __SPIR_FFI_H__
#define __SPIR_FFI_H__

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef size_t DBEntry;

typedef struct {
    const char *data;
    size_t len;
    size_t capacity;
} VecData;

typedef struct {
    void *client;
    VecData pub_params;
} ClientNewRet;

extern void spir_init(uint32_t num_threads);

extern ClientNewRet spir_client_new(uint8_t r);

extern void spir_client_free(void *client);

extern VecData spir_client_preproc(void *client, uint32_t num_preproc);

extern void spir_client_preproc_finish(void *client,
    const char *msgdata, size_t msglen);

extern VecData spir_client_query(void *client, size_t idx);

extern DBEntry spir_client_query_finish(void *client,
    const char *msgdata, size_t msglen);

extern void* spir_server_new(uint8_t r, const char *pub_params,
    size_t pub_params_len);

extern void spir_server_free(void *server);

extern VecData spir_server_preproc_process(void *server,
    const char *msgdata, size_t msglen);

extern VecData spir_server_query_process(void *server,
    const char *msgdata, size_t msglen, const DBEntry *db,
    size_t rot, DBEntry blind);

extern void spir_vecdata_free(VecData vecdata);

#ifdef __cplusplus
}
#endif

#endif
